*************************
Turing Machine Simulator
Designed by Race Mahoney and CeCe Colon
*************************


Running the program is pretty self explanatory. 
Go into the given directory, compile the java program (javac TuringMachine.java), then run the program (java TuringMachine).
This will open up a JOptionPane where you can select the language you want to use.  After selecting, a window will ask for your input string. 
The program will then run in the terminal and alert you whether your string was accepted or denied.


Simple as that.


Enjoy the simulation.



